public class MicrowaveLab4 {
	private double price;
	private String brand;
	private int wattage;
	
	public MicrowaveLab4(){
		this.price=0;
		this.brand = "default";
		this.wattage =0;
	}
	public MicrowaveLab4(double price, int wattage, String brand) //constructor
	{
		this.price = price;
		this.wattage = wattage;
		this.brand = brand;
	}
	
	public void setPrice(double newPrice) {
		this.price = newPrice;
	}
	
	public void setBrand(String newBrand) {
		this.brand = newBrand;
	}
	
	public void setWattage(int newWattage) {
		this.wattage = newWattage;
	}
	
	public double getPrice(){
		return this.price;
	}
	public String getBrand(){
		return this.brand;
	}
	public int getWattage(){
		return this.wattage;
	}
	
	public void sayPrice(double price) {
		System.out.println("I cost: " + price + " $");
	}

	public void makePopcorn(){
		System.out.println("*pop* *pop* *pop* *beep*  *beep* *beep*");
		System.out.println("Enjoy your popcorn");
	}
	
	public void heatForbiddenSoup() {
		System.out.println("mmmmmm mmmmmmm mmmmmm *beep* *beep* *beep*");
		System.out.println("Enjoy your forbidden soup!");
	}
	
	public void changeBrand(String OtherBrand){
		boolean okiedokie = validBrand(OtherBrand);
		
		if(!(okiedokie)){
			this.brand = OtherBrand;
			System.out.println("The brand has been changed to " + this.brand);
		}
		else{
			System.out.println("bad brand name, nuh uh no. No digits in the brand name");
		}
	}

	private boolean validBrand(String newBrandToCheck){
      char[] check = newBrandToCheck.toCharArray();
	  boolean hasDigit = false;
	  
      for(char c : check){
         if(Character.isDigit(c)){
            hasDigit = true;
			break;
         }
		 else{
			hasDigit = false;
		 }
      }
	  return hasDigit;
	}
}