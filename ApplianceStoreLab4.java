import java.util.Scanner;
public class ApplianceStoreLab4 {
	public static void main(String[] args) {
		MicrowaveLab4[] microwaves = new MicrowaveLab4[2];
		Scanner scan = new Scanner(System.in);
		String newBrand = "";
		for(int i = 0; i < microwaves.length; i++){
			microwaves[i] = new MicrowaveLab4();
			
			System.out.println("enter the price of microwave " + i);
			microwaves[i].setPrice(scan.nextDouble());
			
			System.out.println("enter the wattage of microwave " + i);
			microwaves[i].setWattage(scan.nextInt());
			scan.nextLine();
			
			System.out.println("enter the brand of microwave " + i);
			microwaves[i].setBrand(scan.nextLine());
			
			System.out.println(); //line to make it pretty
		}
		System.out.println("previous fields for last microwave");
		System.out.println("Price: " + microwaves[1].getPrice());
		System.out.println("Wattage: " + microwaves[1].getWattage());
		System.out.println("Brand: " + microwaves[1].getBrand());
		
		System.out.println("Enter new price for the last microwave");
		microwaves[1].setPrice(scan.nextDouble());
		
		System.out.println("Enter new wattage for the last microwave");
		microwaves[1].setWattage(scan.nextInt());
			scan.nextLine();
		
		System.out.println("Enter new brand for the last microwave");
		microwaves[1].setBrand(scan.nextLine());
		
		System.out.println("New fields for last microwave");
		System.out.println("Price: " + microwaves[1].getPrice());
		System.out.println("Wattage: " + microwaves[1].getWattage());
		System.out.println("Brand: " + microwaves[1].getBrand());
		
		
		System.out.println("Price of the first microwave: " + microwaves[0].getPrice());
		System.out.println("Wattage of the first microwave: " + microwaves[0].getWattage());
		System.out.println("Brand of the first microwave: " + microwaves[0].getBrand()); 
		
		/* 
		System.out.println("Enter a name to change the brand of the second microwave to");
		newBrand = scan.nextLine();
		
		microwaves[1].changeBrand(newBrand); 
		
		MicrowaveLab4 test = new MicrowaveLab4(300, 1200, "bob");//constructor microwave
		System.out.println("Price of the constructed microwave: " +  test.getPrice());
		System.out.println("Wattage of the constructed microwave: " +test.getWattage());
		System.out.println("Brand of the constructed microwave: " + test.getBrand()); */
		
	}
}